#include "Timer.h"
#include "IntArray.h"
#include <random>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <functional>
#include <fstream>

using Func = std::function<void(IntArray&,size_t)>;
void testAlgorithm(size_t size, const std::string& algorithm, const Func& func);
void SaveResults(const std::string& name, size_t size, double result);

int main()
{

    std::cout << "\nSorting array with Bubblesort." << std::endl;
    testAlgorithm( 5000, "Bubblesort", &IntArray::BubbleSort);

    std::cout << "\nSorting array with Selectionsort." << std::endl;
    testAlgorithm( 5000, "Selectionsort", &IntArray::SelectionSort);

    std::cout << "\nSorting array with Insertionsort." << std::endl;
    testAlgorithm( 5000, "Insertionsort", &IntArray::InsertionSort);

    std::cout << "Sorting array with Quicksort." << std::endl;
    testAlgorithm( 5000, "Quicksort", &IntArray::QuickSortWrapper);


    return 0;
}

void testAlgorithm(size_t size, const std::string& algorithm, const Func& func)
{
    Timer timer;
    IntArray intArray(size);
    double result = 0;

    for( int i = 0; i < 10; ++i)
    {
        intArray.FillArray();
        timer.start();
        func(intArray, size);
        result += timer.stop()/1000000;
    }

    std::cout << "Average sorting time(sec) of " << size << " elements using " << algorithm << ": " << result / 10 << std::endl;
    SaveResults(algorithm, size, result / 10);
    if(size < 40000)
    {
        testAlgorithm(size + 5000, algorithm, func);
    }
}

void SaveResults(const std::string& name, size_t size, double result)
{
    std::fstream file;
    file.open("results.csv", std::ios_base::app);

    if(file.is_open())
    {
        file << name << char(9) << size << char(9) << result << std::endl;
    }
}
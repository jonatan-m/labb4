//
// Created by jonatan on 2018-02-09.
//

#ifndef SORTING_INTARRAY_H
#define SORTING_INTARRAY_H

#include <random>
#include <cstddef>

class IntArray
{
public:
    IntArray();
    explicit IntArray(size_t maxSize);
    IntArray(const IntArray& a);
    ~IntArray();

    size_t GetMaxSize() const { return maxSize; }
    size_t GetSize() const { return size; }
    int GetValue(int index) const;
    bool AddValue(int value);
    void FillArray();
    void Swap(int& R, int& L);
    void QuickSort(int first, int last);

    //so we can use the same function pointer for all sorting algorithms
    void QuickSortWrapper(int size);

    void BubbleSort(int size);
    void SelectionSort(int size);
    void InsertionSort(int size);

private:
    int *arr;
    size_t maxSize;
    size_t size;
    std::random_device rd;
    std::mt19937 rng;
    std::uniform_int_distribution<int> dist;
};


#endif //SORTING_INTARRAY_H

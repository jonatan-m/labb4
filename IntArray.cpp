//
// Created by jonatan on 2018-02-09.
//

#include "IntArray.h"
#include <algorithm>

IntArray::IntArray() : maxSize(0), size(0)
{
    arr = nullptr;
}

IntArray::IntArray(size_t in_maxSize) : maxSize(in_maxSize), size(0), rng(rd()), dist(1, static_cast<int>(maxSize))
{
    arr = new int[maxSize];
}

IntArray::IntArray(const IntArray &a) : maxSize(a.maxSize), size(a.size)
{
    arr = new int[a.maxSize];
    for( size_t i = 0; i < a.size; ++i)
    {
        arr[i] = a.arr[i];
    }
}

IntArray::~IntArray()
{
    delete[] arr;
    arr = nullptr;
}

int IntArray::GetValue(int index) const
{
    return arr[index];
}

bool IntArray::AddValue(int value)
{
    bool valueAdded = false;
    if(size < maxSize)
    {
        arr[size] = value;
        ++size;
        valueAdded = true;
    }
    return valueAdded;
}

void IntArray::FillArray()
{
    std::generate(arr, arr + maxSize, [&] (){ return dist(rng); });
    size = maxSize;
}

void IntArray::QuickSort(int first, int last)
{
    int low = first;
    int high = last;
    int pivot = arr[(first+last)/2];

    do
    {
        while( arr[low] < pivot ) {++low;}

        while( arr[high] > pivot ) {--high;}

        if( low <= high)
        {
            Swap(*(arr+low), *(arr+high));
            ++low;
            --high;
        }
    }while( low <= high);

    if(first < high)
    {
        QuickSort(first, high);
    }
    if(last > low)
    {
        QuickSort(low, last);
    }
}

void IntArray::Swap(int &R, int &L)
{
    int temp = R;
    R = L;
    L = temp;
}

void IntArray::QuickSortWrapper(int size)
{
    QuickSort(0, size);
}

void IntArray::BubbleSort(int size)
{
    for( int pass = 0; pass < size - 1; ++pass)
    {
        for(int i = 0; i < size - 1; ++i)
        {
            if(arr[i] > arr[i + 1])
            {
                Swap(arr[i], arr[i + 1]);
            }
        }
    }
}

void IntArray::SelectionSort(int size)
{
    int smallIndex = 0;

    for(int i = 0; i < size; ++i)
    {
        smallIndex = i;

        for(int j = i + 1; j < size; ++j)
        {
            if(arr[j] < arr[smallIndex])
            {
                smallIndex = j;
            }
        }

        if(smallIndex != i)
        {
            Swap(arr[i], arr[smallIndex]);
        }
    }
}

void IntArray::InsertionSort(int size)
{
    int j;
    int current;
    for( int i = 1; i < size; ++i)
    {
        current = arr[i];
        j = i - 1;

        while( j >= 0 && arr[j] > current)
        {
            arr[j + 1] = arr[j];
            --j;
        }
        arr[j + 1] = current;
    }
}
